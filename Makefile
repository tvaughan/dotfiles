# -*- coding: utf-8; mode: makefile-gmake; -*-

MAKEFLAGS += --no-print-directory --warn-undefined-variables

SHELL := bash
.SHELLFLAGS := -euo pipefail -c

HERE := $(shell cd -P -- $(shell dirname -- $$0) && pwd -P)

.PHONY: all
all: check

.PHONY: has-command-%
has-command-%:
	@$(if $(shell command -v $* 2> /dev/null),,$(error The command $* does not exist in PATH))

.PHONY: is-defined-%
is-defined-%:
	@$(if $(value $*),,$(error The environment variable $* is undefined))

.PHONY: is-repo-clean
is-repo-clean: has-command-git
	@git diff-index --quiet HEAD --

SCRIPTS := tvaughan/.profile.local tvaughan/.zshrc.local $(shell grep -d skip -l bash tvaughan/bin/.percolate.d/* tvaughan/bin/*)

.PHONY: check-static
check-static: has-command-shellcheck
	@shellcheck install $(SCRIPTS)

.PHONY: check-format
check-format: has-command-shfmt
	@shfmt -i 4 -bn -sr -d install $(SCRIPTS)

.PHONY: format
format: has-command-shfmt
	@shfmt -i 4 -bn -sr -w install $(SCRIPTS)

.PHONY: check
check: check-static check-format

.PHONY: install
install: check
	@$(HERE)/install
