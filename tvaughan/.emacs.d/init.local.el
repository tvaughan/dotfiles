;;; package --- Summary:
;;; Commentary:
;;; -*- coding: utf-8; mode: emacs-lisp; lexical-binding: t -*-

;;; Code:

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

(when (file-exists-p custom-file)
  (load custom-file))

;;;

(defun tvaughan/parent-directory (path)
  "Return the parent directory of PATH."
  (or (file-name-directory (directory-file-name path)) ""))

(defun tvaughan/backward-delete-directory (n)
  "Delete characters backward until encountering the end of a word.
Given N, do this n many times."
  (interactive "p")
  (let ((contents (minibuffer-contents)))
    (delete-minibuffer-contents)
    (insert (tvaughan/parent-directory contents))))

;;;

(defun dictionary-lessp (str1 str2)
  "Return t if STR1 is < STR2 when doing a dictionary compare.
Splits the string at numbers and doing numeric compare with them."
  (let ((str1-components (dict-split str1))
        (str2-components (dict-split str2)))
    (dict-lessp str1-components str2-components)))

(defun dict-lessp (slist1 slist2)
  "Compare the two lists of strings & numbers SLIST1 and SLIST2."
  (cond ((null slist1)
         (not (null slist2)))
        ((null slist2)
         nil)
        ((and (numberp (car slist1))
              (stringp (car slist2)))
         t)
        ((and (numberp (car slist2))
              (stringp (car slist1)))
         nil)
        ((and (numberp (car slist1))
              (numberp (car slist2)))
         (or (< (car slist1) (car slist2))
             (and (= (car slist1) (car slist2))
                  (dict-lessp (cdr slist1) (cdr slist2)))))
        (t
         (or (string-lessp (car slist1) (car slist2))
             (and (string-equal (car slist1) (car slist2))
                  (dict-lessp (cdr slist1) (cdr slist2)))))))

(defun dict-split (str)
  "Split the string STR into a list of number and non-number components."
  (save-match-data
    (let ((res nil))
      (while (and str (not (string-equal "" str)))
        (let ((p (string-match "[0-9]*\\.?[0-9]+" str)))
          (cond ((null p)
                 (setq res (cons str res))
                 (setq str nil))
                ((= p 0)
                 (setq res (cons (string-to-number (match-string 0 str)) res))
                 (setq str (substring str (match-end 0))))
                (t
                 (setq res (cons (substring str 0 (match-beginning 0)) res))
                 (setq str (substring str (match-beginning 0)))))))
      (reverse res))))

(defun load-directory (directory)
  "Load *.el in DIRECTORY by natural sort order."
  (add-to-list 'load-path directory)
  (mapc (lambda (name)
          (require (intern (file-name-sans-extension name))))
        (sort (directory-files directory nil "\\.el$") 'dictionary-lessp)))

(defun load-subdirectory (subdirectory)
  "Load *.el in SUBDIRECTORY of `user-emacs-directory'."
  (load-directory (expand-file-name subdirectory user-emacs-directory)))

;;;

(setq radian-font "FiraCode Nerd Font")
(setq radian-font-size 130)

(add-to-list 'radian-disabled-packages 'zerodark-theme)

(radian-local-on-hook after-init
  (defun tvaughan/prescient-tiebreaker (candidate1 candidate2)
    (let ((index1 (cl-search prescient-query candidate1))
          (index2 (cl-search prescient-query candidate2)))
      (if (and (integerp index1) (integerp index2))
          (cond ((< index1 index2) -1)
                ((> index1 index2) 1)
                (t (cond
                    ((< (length candidate1) (length candidate2)) -1)
                    ((> (length candidate1) (length candidate2)) 1)
                    (t 0))))
        (cond
         ((integerp index1) -1)
         ((integerp index2) 1)
         (t 0)))))

  (require 'apheleia)
  (setf (alist-get 'black apheleia-formatters)
        '("ruff" "format" "-"))

  (setq cider-auto-select-error-buffer nil)
  (setq cider-redirect-server-output-to-repl nil)
  (setq cider-reuse-dead-repls 'auto)
  (setq cider-show-error-buffer nil)
  (setq lsp-enable-suggest-server-download nil)
  (setq mac-command-modifier 'meta)
  (setq mac-option-modifier 'none)
  (setq magit-repository-directories '(("~/Projects" . 3)))
  (setq magit-section-visibility-indicator nil)
  (setq magit-show-long-lines-warning nil)
  (setq prescient-filter-method '(literal fuzzy regexp))
  (setq prescient-persist-mode t)
  (setq prescient-sort-full-matches-first nil)
  (setq prescient-sort-length-enable t)
  (setq prescient-tiebreaker #'tvaughan/prescient-tiebreaker)
  (setq rg-ignore-ripgreprc nil)
  (setq vertico-cycle t)
  (setq web-mode-engines-alist '(("django" . "\\.html\\'")))

  ;; https://github.com/radian-software/radian/commit/5ab1ccdc6df5af6fd8fbc065b64f289848a49b96
  (keymap-set vertico-map "DEL" #'vertico-directory-delete-char)

  ;; https://stackoverflow.com/a/1942422
  (mapc 'load-subdirectory '("lisp")))
