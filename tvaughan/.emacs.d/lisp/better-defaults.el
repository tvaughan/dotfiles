;;; package --- Summary:
;;; Commentary:
;;; -*- coding: utf-8; mode: emacs-lisp; lexical-binding: t -*-

;;; Code:

(setq-default fill-column 78)

(setq dired-listing-switches "-AFl --group-directories-first --time-style=long-iso")
(setq enable-remote-dir-locals t)
(setq ibuffer-expert t)
(setq ispell-dictionary "en_US")
(setq ispell-hunspell-dictionary-alist '(("en_US" "[[:alpha:]]" "[^[:alpha:]]" "[']" t ("-d" "en_US") nil utf-8)))
(setq ispell-hunspell-dict-paths-alist '(("en_US" "/usr/share/hunspell/en_US.aff")))
(setq ispell-program-name "hunspell")
(setq ispell-really-hunspell t)
(setq next-line-add-newlines t)

(global-set-key (kbd "C-x C-b") 'ibuffer)

(defun tvaughan/kill-this-buffer ()
  "Kill this buffer."
  (interactive)
  (kill-buffer (current-buffer)))

(global-set-key (kbd "C-x k") #'tvaughan/kill-this-buffer)
(global-set-key (kbd "C-x w") #'delete-frame)

(define-minor-mode tvaughan/pinned-buffer-mode
  "Pin the current buffer to the selected window."
  :init-value nil
  :lighter (" PB")
  (let (window (selected-window))
    (set-window-dedicated-p window #'tvaughan/pinned-buffer-mode)
    (set-window-parameter window 'no-delete-other-windows #'tvaughan/pinned-buffer-mode)))

(global-set-key (kbd "C-c p") #'tvaughan/pinned-buffer-mode)

(defun tvaughan/previous-window ()
  "Corollary to 'other-window'."
  (interactive)
  (other-window nil))

(global-set-key (kbd "C-x p") #'tvaughan/previous-window)
(global-set-key (kbd "C-x n") #'other-window)

(defun tvaughan/sort-words (reverse beginning end)
  "Sort words (non-whitespace strings) in region, in REVERSE if negative, from BEGINNING to END."
  (interactive "*P\nr")
  (sort-regexp-fields reverse "\\(\\S-\\)+" "\\&" beginning end))

(defun tvaughan/window-enlarge-horizontally (columns)
  (enlarge-window columns t))

(defun tvaughan/window-shrink-horizontally (columns)
  (shrink-window columns t))

(defun tvaughan/window-set-width (columns)
  (interactive "nEnter new window width (in columns): ")
  (tvaughan/window-enlarge-horizontally (- columns (window-width)))
  (message "New window width is: %d" (window-width)))

(defun tvaughan/window-enlarge-vertically (rows)
  (enlarge-window rows nil))

(defun tvaughan/window-shrink-vertically (rows)
  (shrink-window rows nil))

(defun tvaughan/window-set-height (rows)
  (interactive "nEnter new window height (in rows): ")
  (tvaughan/window-enlarge-vertically (- rows (window-height)))
  (message "New window height is: %d" (window-height)))

(defun tvaughan/untabify ()
  "Preserve initial tab when makefile-mode."
  (interactive)
  (save-excursion
    (if (derived-mode-p 'makefile-mode)
        (progn
          (goto-char (point-min))
          (while (not (eobp))
            (skip-chars-forward "\t")
            (untabify (point) (line-end-position))
            (forward-line 1)))
      (untabify (point-min) (point-max)))))

(add-hook 'before-save-hook #'tvaughan/untabify)

(straight-use-package 'uuid)
(require 'uuid)

(defun tvaughan/uuid ()
  "Return a lowercase UUID."
  (downcase (uuid-string)))

(defun tvaughan/insert-uuid ()
  "Insert a lowercase UUID at point and add it to the kill ring."
  (interactive)
  (let ((uuid (tvaughan/uuid)))
    (kill-new uuid)
    (insert uuid)))

(global-set-key (kbd "C-c u") #'tvaughan/insert-uuid)

(straight-use-package 'cider-storm)

(straight-use-package 'define-word)
(global-set-key (kbd "C-c d") #'define-word-at-point)
(global-set-key (kbd "C-c D") #'define-word)

(straight-use-package 'direnv)
(direnv-mode t)

(straight-use-package 'expand-region)
(global-set-key (kbd "C-=") #'er/expand-region)

(straight-use-package 'nix-mode)
(add-to-list 'auto-mode-alist '("\\.nix\\'" . nix-mode))

(straight-use-package 'olivetti)
(setq olivetti-lighter nil)

(straight-use-package 'popper)
(setq popper-display-control nil)
(setq popper-display-function #'display-buffer-in-direction)
(setq popper-reference-buffers
      '("Output\\*$"
        "^\\*Async Shell Command"
        "^\\*Choices"
        "^\\*HTTP Response"
        "^\\*Messages"
        "^\\*cider-repl"
        help-mode
        compilation-mode))
(global-set-key (kbd "C-,") #'popper-toggle)
(global-set-key (kbd "C-.") #'popper-cycle)
(global-set-key (kbd "C-M-,") #'popper-toggle-type)
(popper-mode t)
(popper-echo-mode t)

(straight-use-package 'rainbow-mode)
(add-hook 'text-mode-hook #'rainbow-mode)

(straight-use-package 'restclient)
(require 'restclient)

(straight-use-package 'savehist)
(savehist-mode)

(with-eval-after-load "mm-decode"
  (add-to-list 'mm-discouraged-alternatives "text/html")
  (add-to-list 'mm-discouraged-alternatives "text/richtext"))

(straight-use-package 'lsp-mode)
(require 'lsp-mode)

(setq lsp-auto-register-remote-clients t)

(add-hook 'clojure-mode-hook 'lsp)
(add-hook 'clojurescript-mode-hook 'lsp)
(add-hook 'clojurec-mode-hook 'lsp)

(dolist (m '(clojure-mode
             clojurec-mode
             clojurescript-mode
             clojurex-mode))
  (add-to-list 'lsp-language-id-configuration `(,m . "clojure")))

(straight-use-package 'lsp-ui)
(add-hook 'lsp-mode-hook 'lsp-ui-mode)

(provide 'better-defaults)
;;; better-defaults.el ends here
