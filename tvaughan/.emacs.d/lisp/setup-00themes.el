;;; package --- Summary:
;;; Commentary:
;;; -*- coding: utf-8; mode: emacs-lisp; lexical-binding: t -*-

;;; Code:

;; (straight-use-package 'base16-theme)
;; (load-theme 'base16-catppuccin-macchiato t)

(straight-use-package 'catppuccin-theme)
(setq catppuccin-flavor 'macchiato)
(load-theme 'catppuccin t)

(straight-use-package 'spacious-padding)
(setq spacious-padding-subtle-mode-line
      '(:mode-line-active default :mode-line-inactive "#434C5E"))
(spacious-padding-mode t)

(provide 'setup-00themes)
;;; setup-00themes.el ends here
